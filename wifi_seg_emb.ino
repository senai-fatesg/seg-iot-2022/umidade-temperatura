#include "DHT.h" //1.3.8
#include <WiFi.h>
#include <PubSubClient.h>

const char* ssid = "IOT";
const char* password = "segiotmais";

const char* BROKER_MQTT = "192.168.77.41";
int BROKER_PORT = 1883; // Porta do Broker MQTT

unsigned long ultimoTempo = 0;

const char* TOPIC = "id_disp/temp";

WiFiClient espClient; // Cria o objeto espClient
PubSubClient MQTT(espClient); // Instancia o Cliente MQTT passando o objeto espClient

DHT dht(5, DHT11);

void setup() {
  dht.begin();
  Serial.begin(115200);
  initWiFi();

}

void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}

void reconnectMQTT(void) {
    while (!MQTT.connected()) {
        MQTT.setServer(BROKER_MQTT, BROKER_PORT);   //informa qual broker e porta deve ser conectado
      
        Serial.print("* Tentando se conectar ao Broker MQTT: ");
        Serial.println(BROKER_MQTT);
        if (MQTT.connect("esp32")) {
            Serial.println("Conectado com sucesso ao broker MQTT!");
            MQTT.subscribe("aula/fcxexemplo04");
        } else {
            Serial.println("Falha ao reconectar no broker.");
            Serial.println("Havera nova tentatica de conexao em 2s");
            delay(2000);
        }
    }
    MQTT.loop();
}

void loop() {
  reconnectMQTT();
  if(millis() - ultimoTempo > 5000){
    ultimoTempo = millis();
    float t = dht.readTemperature();
    String msg = "Temp: " + String(t);
    MQTT.publish(TOPIC, msg.c_str());
    Serial.println(msg);
  }
  
}
